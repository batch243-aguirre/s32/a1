const http = require("http");
const port = 4000;

http
  .createServer((req, res) => {

    if (req.url === "/" && req.method == "GET") {

      res.writeHead(200, { "Content-type": "text/plain" });

      res.end("Welcome to Booking System");

    } else if (req.url === "/profile" && req.method == "GET") {

      res.writeHead(200, { "Content-type": "text/plain" });

      res.end("Welcome to your profile!");

    } else if (req.url === "/courses" && req.method === "GET") {

      res.writeHead(200, { "Content-type": "text/plain" });
     
      res.end("Here's our courses available.");

    } else if (req.url === "/course" && req.method === "POST") {
      
      res.writeHead(200, { "Content-type": "text/plain" });
      
      res.end("Add a course to our resources.");

    } 
  })
  .listen(port);

console.log(`Server running at port ${port}`);
